# lombere-magic.zsh-theme
# This theme is herited from af-magic (Repo here: https://github.com/andyfleming/oh-my-zsh)
# with some personalisations

# settings
typeset +H return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
typeset +H my_gray="$FG[237]"
typeset +H my_orange="$FG[214]"

# separator dashes size
function afmagic_dashes {
	local PYTHON_ENV="$VIRTUAL_ENV"
	[[ -z "$PYTHON_ENV" ]] && PYTHON_ENV="$CONDA_DEFAULT_ENV"

	if [[ -n "$PYTHON_ENV" && "$PS1" = \(* ]]; then
		echo $(( COLUMNS - ${#PYTHON_ENV} - 3 ))
	else
		echo $COLUMNS
	fi
}

function displayColors {
	for (( i = 0; i < ${#FG[@]}; i++ )); do
		if [[ $i -lt 10 ]]; then
			echo "$FG[00$i]Color $i $reset_color"
		elif [[ $i -lt 100 ]]; then
			echo "$FG[0$i]Color $i $reset_color"
		else
			echo "$FG[$i]Color $i $reset_color"
		fi
	done
}

PATH_COLOR="$FG[032]"
USER_COLOR="$FG[237]"
if [[ "$USER" == "root" ]]; then
 	PATH_COLOR="$FG[166]$FX[italic]$FX[underline]"
	USER_COLOR="$FG[172]$FX[underline]$FX[blink]"
fi

# primary prompt
PS1='$FG[237]${(l.$(afmagic_dashes)..-.)}%{$reset_color%}
'$PATH_COLOR'%4~'$FX[reset]'$(git_prompt_info)$(hg_prompt_info) $FG[105]%(!.#.»)%{$reset_color%} '
PS2='%{$fg[red]%}\ %{$reset_color%}'
RPS1='${return_code}'

# right prompt
(( $+functions[virtualenv_prompt_info] )) && RPS1+='$(virtualenv_prompt_info)'
RPS1+=' '$USER_COLOR'%n'$FX[reset]'$my_gray@%m%{$reset_color%}%'

# git settings
ZSH_THEME_GIT_PROMPT_PREFIX="$FG[075]($FG[078]"
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_DIRTY="$my_orange*%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="$FG[075])%{$reset_color%}"

# hg settings
ZSH_THEME_HG_PROMPT_PREFIX="$FG[075]($FG[078]"
ZSH_THEME_HG_PROMPT_CLEAN=""
ZSH_THEME_HG_PROMPT_DIRTY="$my_orange*%{$reset_color%}"
ZSH_THEME_HG_PROMPT_SUFFIX="$FG[075])%{$reset_color%}"

# virtualenv settings
ZSH_THEME_VIRTUALENV_PREFIX=" $FG[075]["
ZSH_THEME_VIRTUALENV_SUFFIX="]%{$reset_color%}"
