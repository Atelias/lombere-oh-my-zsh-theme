# lombere oh my zsh theme

# Installation

To install this theme simply run this command :
```sh
wget https://gitlab.com/Atelias/lombere-oh-my-zsh-theme/-/raw/master/lombere.zsh-theme -O ~/.oh-my-zsh/themes/lombere.zsh-theme
```

Then change the `ZSH_THEME` variable in your `~/.zshrc` to `lombere`. 
So the line become : 
```
ZSH_THEME="lombere"
```

Then restart your shell or simply do 
```
source ~/.zshrc
```
